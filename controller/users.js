//[SECTION] Depencies and Modules 
	const User = require('../models/User'); 
	const Course = require('../models/Course'); 	
	const bcrypt = require('bcrypt');	
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

//[SECTION] Environment Variables Setup
	dotenv.config();
	const salt = Number(process.env.SALT);

//[SECTION] Functionalities [CREATE]  
	//1. Register New Account.
	//**
	module.exports.register = (userData) => {
	 	let fName = userData.firstName;
	 	let lName = userData.lastName;
	 	let email = userData.email;
	 	let passW = userData.password;
	 	let mobil = userData.mobileNo;
	 		 	
	 	let newUser = new User({
	 		firstName: fName,
	 		lastName: lName,
	 		email: email,
	 		password: bcrypt.hashSync(passW, salt),
	 		mobileNo: mobil
	 	});	 	
		return newUser.save().then((user, err) => {			
			if (user) {
				return user;
			} else {
				return 'Failed to Register account'
			}; 
		});
	};

//User Authentication
/*
Steps:
	1. Check the database if the user email exists
	2. Compare the password provided int the login form with password stored in the database.
	3. Generate/return a JSON web token if the user is successfully  logged in, and return false if not.
*/
//*
module.exports.loginUser = (data) => { //data is from the req.body
	//use findOne method to return the first method in the collection that matches the search criteria.
	return User.findOne({ email: data.email }).then(result => {
	//data.email - actual data coming from the req.body
		//if User does not exist
		if (result == null) {
			return false;
		} else {
			//User exists, match the password in the datbase and and the password provided in req.body
			//compareSync - method from the bcrypt to used in comparing the non encrypted password from the login and the datbase password. It returns true ot false depending on the result.
			const isPasswordCorrect =bcrypt.compareSync(data.password, result.password)
			//is (isPasswoerCorrect) stores boolean
			//if the password match, return token,
			if(isPasswordCorrect){
				return {accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false
			}
		}
	})
};

//[SECTION] Functionalities [RETREIVE] retreive the user'details.
/*
STEPS:
	1. Find the document in the database uisng the user's ID.
	2. Reassign the password of the returned document to an empty string.
	3. Return the result back to the client
*/

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		//change the value of the user's password to an empty string.
		result.password = '';
		return result;
	})
}

//Enroll the Registered Users
/*
Enrollment Steps:
	1. Look for the user by their ID.
		-push the details of the course we're trying to enroll in. We'll push the details to a new enrollment subdocument in our user.
	2. Look for the course by its ID.
		-push the details o fthe enrollee/user who's trying to enroll .
		-we'll push to a new enrollee's subdocument in our course
	3. When both saving documents are successful, we send  mesage to client.
*/

module.exports.enroll = async (req,res) => {
	//console.log('Test enroll route');
	console.log(req.user.id);//from verified user's id appears in the console
	console.log(req.body.courseId)//course ID from req body appears in the console
	//admin cannot enroll
	if (req.user.isAdmin) {
		return res.send ({message: "action forbidden"})
	} 

//Get the user's ID to save the courseId inside the enrollments field
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		//add the courseId in an object and push that object into user's enrollments array:
		let newEnrollment = {
			courseId: req.body.courseId
		}
		user.enrollments.push(newEnrollment);
		//save the changes made to our user document
		return user.save().then(user => true).catch(err => err.message)
		//conditional statement for the catch
		//if isUserUpdated does not contain the boolean true, we will stop our process and return a message to our client.
		if(isUserUpdated !== true) {
			return res.send({message: isUserUpdated})
		}
	});

	//Fimf the course ID that we will need to push to our enrollee.
	let isCourseUpdated = await Course.findById(req.body.courseId).then(
		course => {
			//Create an object which will be push to enrollees array/field
			let enrollee = {
				userName: req.user.name,
				userId: req.user.id
			}
			course.enrollees.push(enrollee);
			//save the course document
			return course.save().then(course => true).catch(err => err.message)

			if(isCourseUpdated !== true) {
				return res.send({message: isCourseUpdated})
			}
		})

	//send message to the client that we have successfully enrolled the user if both isUserUpdated and isCourseUpdated contain the boolean true.

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Good Luck, You are Enrolled"})
	}
}

//Get user's enrollments

module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id)
		.then(result => res.send(result.enrollments))
		.catch(err => res.send(err))
}