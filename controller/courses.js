const Course = require('../models/Course');
//Create a new course
/*
STEPS:
	1. Create a new course object using the mongoose model and the information from the requets body
	2. Save the new course to the database.
*/
module.exports.addCourse = (reqBody) => {
	//reqBody -> where the course information will come from.
	//create a variable "newCoaurse" and instantiate the name, ddescription and price.

	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//save the created object to our database
	return newCourse.save().then((course, error) => {
		if (error) {
			return false
		} else {
			return course
		}
	}).catch(error => error)//for terminal not to crush whenever there is an error.
	/*.catch(error => error.message)*/
}

//Retreive all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result})
}

//Retreive AlL active users
//1. Retreive all the courses with the property isActive:true
module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result => {
		return result
	})
}

//Retrieving a specic course
//1. Retrieve the course that matches the course ID provided from the URL

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	}).catch(error => error)
}

//UPDATE a course
/*
Steps:
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req.body
	2. Find and Update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body

*/

  module.exports.updateCourse = (courseId, data) => {
    //specify the fields/properties of the document to be updated
    let updatedCourse = {
      name: data.name,
      description: data.description,
      price: data.price
    }

    //findByIdAndUpdate(document Id, updatesToBeApplied)
    return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
      if(error){
        return false;
      } else {
        return course;
      }
    }).catch(error => error)
  }


//Archiving a Course
//1.Update the status of "isActive" into "false" which will no longer be displayedin the client whenever all active courses are retreive.
module.exports.archiveCourse = (courseId) => {
	let updateActiveField = {
		isActive: false
	};
	return Course.findByIdAndUpdate(courseId, updateActiveField).then((course,error) => {
		if (error) {
			return false
		} else {
			return course
		}
	}).catch(error => error)
}

//Delete a Course 
module.exports.deleteCourse = (courseId) => {
	return Course.findByIdAndRemove(courseId).then((course,error) => {
		if (error) {
			return {message: 'An error happened'}
		} else {
			return {message: 'Course successfully deleted'}
		}
	})
}