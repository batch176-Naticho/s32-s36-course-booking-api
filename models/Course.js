//[SECTION] Depencies and Modules
const mongoose = require('mongoose')

//[SECTION] Schema/Blueprints
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required:[true, 'Course Name is Required']
	},
	description: {
		type: String,
		required:[true, 'Course Description is Required']
	},
	price: {
		type: Number, 
		required: [true, 'Course price is Required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userName:String,
			userId: {
				type: String,
				required: [true, 'Student ID is required']
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			}
		}
	]
});

//[SECTION] Models
	module.exports = mongoose.model('Course', courseSchema);
