const jwt = require('jsonwebtoken');

//secret is a passcode only the server and the jwt knows this secret.
//User defined string data that will be used to create our JSON web tokens.
//used in algorithm for encrypting our data which makes it difficult to decode the information without tge defined secret keyword
const secret = 'CourseBookingAPI';//passcode

//JWT is a way to securely pass information from one part of the server to the frontend or other parts of our application. this will allow us to authorize oir users to access or disallow access to certain parts of our app.


//Token Creation
//Analogy: Pack the gift and provide a lock with the secret code as the key.

//*controller users
module.exports.createAccessToken = (user) => {//check if the user and if the password is the same
	//check if we can receive the details of the user from our login.
	console.log(user);// kung may papasok na user datails, appears in the terminal

	//object to conatin some details of the user.
	//with _id the other details wil also be accessible.
	//this will be the data in the payload.
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//Generate JSON web token using the jwt's sign method
	return jwt.sign(data, secret, {})
	//return jwt.sign(data, secret, {expiresIn: '1hr'}) expiresIn: '1hr' - will log out in 1 hr.
}

//Token verification
//Analogy: Receive the gift and open the lock to verify if the sender is legitimate and the gift is not tampered.

module.exports.verify = (req,res, next) => {
	//next middleware that has access to req and res
	//token is retreived from the request
	console.log(req.headers.authorization)
	//.authorization -> where we get our token

	let token = req.headers.authorization;
	//if statement will first check if the token variable contains undefined or a proper jwt. If its undefined, we will check token's data type typeof, then send a message to the client.
	if (typeof token === "undefined") {
		return res.send ({auth: "Failed. No token"})
	} else {
		console.log(token)
		//Bearer ldmfmlcdspkfkzfj
		//slice(<startingPosition>,<endPosition>)
		token = token.slice(7, token.length )
		// 7 -> index before arriving to the actual token including the space (Bearer<space> = 7)

		//validate the token using the "verify" method decrypting the token using the secret code.
		jwt.verify(token,secret, function(err, decodedToken) {
			//"decodedToken" - an object containing the data of the user that matched the token (from when the user login was successful).
			//err -> will contain the error from decoding your token. This will contain the reason why we will reject the token
			//if verification of token is success, then jet.verify will return the decoded token
			if (err) {
				res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				console.log(decodedToken)// contains the data from our token.
				req.user = decodedToken
				//user property will be added to request object and will contain ou decodedToken.
				next();// will let us proceed to the next middleware or controlller. will continue to route.get('/details',auth.verify, (req,res) => {
					//controller.getProfile(req.user.id).then(result => res.send(result))

			}
		})
	}
}


//Verify admin and will be used also as a middleware.

module.exports.verifyAdmin = (req,res,next)  => {
	//we can get details from req.user because verifyAdmin comes after verify method
	//Note: You can only have req.user for any middelware or controller that comes after verify.
	if (req.user.isAdmin) {
		//if the logged user, based on his token is an admin, we will proceed to the next middleware/controller
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}

