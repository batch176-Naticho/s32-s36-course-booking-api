const express = require('express');
const route = express.Router();
const CourseController = require('../controller/courses');
const auth = require('../auth');

//destructure the actual function that we need to use.
const {verify, verifyAdmin} = auth;

//Routes for creating a course
route.post('/createCourse',verify, verifyAdmin, (req,res) => {
	CourseController.addCourse(req.body).then(result => res.send(result))
})

//Retreive all courses
route.get('/allCourse',verify,verifyAdmin, (req,res) => {
	CourseController.getAllCourses().then(result => res.send(result))
})// verify -> those who are logged in
//verifyAdmin -> isAdmin: true

//Retreive all active courses
route.get('/activeCourse', (req,res) => {
	CourseController.getAllCourses().then(result => res.send(result))
})//anyone can access logged in or not, admin or not.

module.exports = route;

//Retrieving a SPECIFIC course
//req.params (is short for a parameter)
// " /:parameterName"
route.get('/:courseId', (req, res) => {
	console.log(req.params.courseId)
	//we can retrieve the course ID by accessing the request's "params" property which contains all the paramteres provided via the URL
	CourseController.getCourse(req.params.courseId).then(result => res.send(result));
})

//Route for UPDATING a course
route.put('/:courseId', verify, verifyAdmin, (req, res) => {
	CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
})//req.body -> updated course
//req.params.id -> reference

//Archiving a Course
route.put('/:courseId/archive',verify,verifyAdmin, (req,res) => {
	CourseController.archiveCourse(req.params.courseId).then(result => res.send(result));
});

//Delete a Course