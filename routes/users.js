//[SECTION] Depencies and Modules
	const exp = require ('express') 
	const controller = require ('../controller/users') 
	const auth = require('../auth')

//[SECTION] Routing Component
	const route = exp.Router(); 
	
//[SECTION] Routes - POST
//** controller
	route.post('/register', (req,res) => {
		//console.log(req.body);
		let userData = req.body;// userData -> data/information that the user/client provided, in this case information is produced in the postman's body. 
		//userData is a function from the controller.  
		controller.register(userData).then(outcome => {
			res.send(outcome)
			// if successful, produced data from the postman will reflect in the postman's console.
			//'register' contains infromation the user/client will provide, structured in the schema.

		})
	});

//[SECTION] Route for User Authentication (login)

route.post('/login', (req,res) => {
	controller.loginUser(req.body).then(result => res.send(result))
});

//[SECTION] Routes - GET the user's details
	/*route.post('/details', (req,res) => {
		controller.getProfile({userId: req.body.id}).then(result => res.send(result))
		//getProfile(userId: req.body.id) = getProfile(req.body) eitherway is ok.
		//getProfile(req.body) -> in controller User.findById(data.id)
		//we will add an actual id in postman, Id from mongoDB
	})*/
	//after getting the token we will replace the the post to get
	route.get('/details',auth.verify, (req,res) => {
		controller.getProfile(req.user.id).then(result => res.send(result))

})
//[SECTION] Routes - PUT
//[SECTION] Routes - DEL
//[SECTION] Expose Routing System
	module.exports = route; 

//Enroll the Registered Users
//Only the verified user can enroll in a course.
route.post('/enroll', auth.verify, controller.enroll);

//Get logged user's enrollments
route.get('/getEnrollments', auth.verify, controller.getEnrollments);